\part[author={\protect\insertauthor}]{Design and Implementation of an Agent Platform}\label{chap:platform}

\begin{graphicspathcontext}{{./chapters/platform/imgs/},{./chapters/platform/imgs/auto/},{./chapters/intro/1sarl/imgs/auto/},\old}

\begin{frame}{Goals of this Lecture}
	\begin{enumerate}
	\item Presentation of the basics of an agent platform.
		\begin{itemize}
		\item Standard features.
		\item Standard architecture.
		\end{itemize}
	\item Description the key points for designing an agent platform.
	\item Overview of the major agent platforms over the World.
	\item Examples:
		\begin{itemize}
		\item The TinyMAS platform.
		\item The Janus platform.
		\end{itemize}
	\end{enumerate}
\end{frame}

\tableofcontentslide

\section{Introduction}

\begin{frame}{Standards}
	\begin{block}{Two main standards in MAS platforms}
	\begin{itemize}
	\item FIPA\footnote{FIPA, Foundation for Intelligent Physical Agents~: \url{http://www.fipa.org}}\\
	Reference Implementation~: JADE~\begin{footnotesize}\citep{jade}\end{footnotesize}
	\item MAF\footnote{MAF, Mobile Agent Facility~: \url{http://www.omg.org/technology/documents/formal/mobile_agent_facility.htm}}\\
	Reference Implementation~: GrassHopper~\begin{footnotesize}\citep{Baumer.99,Baumer.00}\end{footnotesize}
	\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Agent-oriented Platforms}
  \begin{block}{Classification}
  \begin{description}
  \item[Language-oriented]
	  \begin{itemize}
	  \item Declarative languages: CLAIM, DALI ou ReSpecT.
	  \item Imperative languages: Jack. 
	  \item Hybrid : Jason ou IMPACT.
	  \end{itemize}
  \item[Mobile or Web Agents] Jade, AgentBuilder, GrassHopper, Aglet, Able.
  \item[Simulation] NetLogo, GAMA, Matsim, Swarm, Cormas, Mobidyc.
  \item[General platforms] MAST, Geamas, Madkit, Magique, Janus (SARL).
  \end{description}
  \end{block}
  \begin{center}
  \alert{Many platforms have been created, few have survived}
  \end{center}
\end{frame}

\sidenote{This table was done according to experiments with my students.}
\begin{frame}[b]{{Comparing SARL} to Other Frameworks}
	\tiny
	\begin{tabularx}{\linewidth}{|X|X|X|X|X|X|X|X|X|}
		\hline
		\tabularheading\chead{Name} & \chead{Domain} & \chead{Hierar.\textup{a}} & \chead{Simu.\textup{b}} & \chead{C.Phys.\textup{c}} & \chead{Lang.} & \chead{Beginners\textup{d}} & \chead{Free} \\
		\hline
		\hline
		GAMA & Spatial simulations & & \checkmark & & GAML, Java & **[*] & \checkmark \\
		\hline
		Jade & General & & \checkmark & \checkmark & Java & * & \checkmark \\
		\hline
		Jason & General & & \checkmark & \checkmark & Agent\-Speaks & * & \checkmark \\
		\hline
		Madkit & General & & \checkmark & & Java & ** & \checkmark \\
		\hline
		NetLogo & Social/ natural sciences & & \checkmark & & Logo & *** & \checkmark \\
		\hline
		Repast & Social/ natural sciences & & \checkmark & & Java, Python, .Net & ** & \\
		\hline
		\rowcolor{CIADlightmagenta} SARL & General & \checkmark & \checkmark\textup{e} & \checkmark & SARL, Java, Xtend, Python & **[*] & \checkmark \\
		\hline
	\end{tabularx}
	\vspace{2em}
	\TINY
	\begin{enumerate}[a]
	\item Native support of hierarchies of agents.
	\item Could be used for agent-based simulation.
	\item Could be used for cyber-physical systems, or ambient systems.
	\item *: experienced developers; **: for Computer Science Students; ***: for others beginners.
	\item Ready-to-use Library: Jaak Simulation Library, \url{https://github.com/gallandarakhneorg/jaak}
	\end{enumerate}
\end{frame}

\section[Platforms]{{Brief Listing} of Existing Platforms}

\begin{frame}{Jade}
	\wrapfigure[width=.15\linewidth]{jadelogo}
	\begin{itemize}
	\wrapitem{Agent platform}
	\wrapitem{Agent-agent communications}
	\wrapitem{Multi-thread and execution policies}
	\item Network support
	\item Agent observation toolkit
	\item BDI, Language Acts
	\item FIPA Std reference implementation
	\item \alert{Environment Model}: none
	\end{itemize}
	\begin{center}
	\url{http://jade.tilab.com}
	\end{center}
\end{frame}

\begin{frame}{NetLogo}
	\wrapfigure[width=.2\linewidth]{netlogologo}
	\begin{itemize}
	\wrapitem{Multi-agent platform}
	\wrapitem{Agent = turtle}
	\wrapitem{Turtle communication: direct and stigmergy}
	\item Multi-thread and execution policies
	\item Observation toolkit
	\item \alert{Environment Model}: Patches and Links.
	\end{itemize}
	\begin{center}
	\url{http://ccl.northwestern.edu/netlogo}
	\end{center}
\end{frame}

\begin{frame}{Gama}
	\wrapfigure[width=.2\linewidth]{gamalogo}
	\begin{itemize}
	\wrapitem{Spatial simulation}
	\wrapitem{Agent-agent communications}
	\wrapitem{Multi-thread and execution policies}
	\item Network support
	\item Agent observation toolkit
	\item \alert{Environment Model}: Geographical data structures.
	\end{itemize}
	\begin{center}
	\url{https://github.com/gama-platform}
	\end{center}
\end{frame}

\begin{frame}{Janus}
	\wrapfigure[width=.15\linewidth]{janus}
	\begin{itemize}
	\wrapitem{Agent and organizational platform}
	\wrapitem{Agent-agent communications}
	\wrapitem{Multi-thread and execution policies}
	\item Transparent network support with Hazelcast and ZeroMQ
	\item Agent observation toolkit
	\item Maven compliant
	\item BDI, Language Acts, Android
	\item \alert{Environment Model}: Jaak extension (open source), JaSim, both based on Perception-Influence-Reaction Models
	\end{itemize}
	\begin{center}
	\url{http://www.janusproject.io}
	\end{center}
\end{frame}

\begin{frame}[t]{{Some other} platforms}
	\footnotesize
	\begin{description}
	\item[Agentbuilder] \url{http://www.agentbuilder.com}
	\item[Cougaar] \url{http://www.cougaar.org}
	\item[Goal] \url{http://mmi.tudelft.nl/trac/goal}
	\item[JaCaMo] \url{http://jacamo.sourceforge.net} and its components: Jason, CArtAgO, Moise
	\item[Jack] \url{http://www.agent-software.com/products/jack/}
	\item[Jadex] \url{http://jadex-agents.informatik.uni-hamburg.de}
	\item[JIAC] \url{http://www.jiac.de}
	\item[Madkit] \url{http://www.madkit.org}
	\item[Mason (sugarscape)] \url{http://cs.gmu.edu/~eclab/projects/mason/}
	\item[Matsim] \url{http://matsim.org}
	\item[Repast] \url{http://repast.sourceforge.net}
	\item[Soar] \url{http://sitemaker.umich.edu/soar/home} and \url{http://www.soartech.com}
	\item[TinyMAS] \url{http://www.arakhne.org/tinymas/}
	\item[Whitestein LS] \url{http://www.whitestein.com}
	\end{description}
\end{frame}

\restricttoc<2-3>
\section[Platform from Scratch]{Agent Platform From Scratch: the example of the TinyMAS platform}

\begin{frame}{{TinyMAS} Platform}
	\begin{itemize}
	\item Designed for teaching basics of the MAS platform design to students.
	\item It enables to create simple agent-based simulation applications.
	\item It is not designed for ``serious'' applications.
	\item No more under development since all the developers are involed into the Janus project.
	\end{itemize}
	\vfill
	\begin{center}
		Official website: \\
		\url{http://www.arakhne.org/tinymas/} \\[1em]
		Source code: \\
		\url{https://github.com/gallandarakhneorg/tinymas/}
	\end{center}
\end{frame}

\begin{frame}{Properties of the TinyMAS Platform}
	\begin{block}{What it can do}
		\begin{itemize}
		\item Easy to create reactive agents.
		\item Communication between agents by messages.
		\item Synchronous execution of the agents.
		\item Framework for the physic environment.
		\end{itemize}
	\end{block}
	\vspace{1em}
	\begin{alertblock}{What it cannot do}
		\begin{itemize}
		\item Hard to create rational agents.
		\item Asynchronous execution of agents.
		\item Cannot be easily deployed on a computer network.
		\end{itemize}
	\end{alertblock}
\end{frame}

\subsection{Agent Identification}

\begin{frame}[t]{Agent Identification}
	\smaller
	\begin{alertblock}{Needs}
		Agents must be identified in a unique way for receiving the messages.
	\end{alertblock}
	\begin{block}{How to make it unique?}
		\begin{itemize}
		\item Agent identifiers are based on an UUID.
		\item Each identifier is linked to the identifier of the kernel.
		\item A name could be associated to the agent identifier, but it may be not unique.
		\end{itemize}
	\end{block}
	\begin{center}
		\includegraphics[width=.6\linewidth]{tinymas_identification}
	\end{center}
\end{frame}

\begin{frame}[t]{Agent Identification \insertcontinuationtext}
	\small
	\begin{alertblock}{Problem of the reference to the kernel identifier}
		If the agent identifiers are binded to the kernel identifiers, the agents are not able to migrate from one kernel to another.
	\end{alertblock}
	\begin{block}{How to make it possible?}
		Assuming UUID is unique in all the computers over the World, use only the UUID.
	\end{block}
	\begin{center}
		\includegraphics[width=.6\linewidth]{tinymas_identification2}
	\end{center}
\end{frame}

\subsection{Time Manager}

\begin{frame}{Time: Why it is important?}
	\begin{definition}[Definition of Simulation \citep{Banks99}]
		The simulation is the \Emph{imitation in time} of the operations of a real or imaginary process. The simulation involves the generation of an artificial evolution of the system and the observation of these developments to make inferences about the operational characteristics of the real system represented.
	\end{definition}
	\vspace{2em}
	\alertbox{How to model the time? \\ How to manage the time evolution?}
\end{frame}

\figureslide{Typical Timelines}{timelines}

\figureslide[label=synchronousexecutionslide]{Synchronous Agent Execution}{scheduling}

\begin{frame}<1-4>[t]{{Time Manager} API}
	\footnotesize
	\begin{description}
	\item Abstract implementation that provides basics implementation.
	\item[preAgentScheduling] called at the beginning of a simulation step (before any agent execution) in a synchronous execution engine.
	\item[postAgentScheduling] called at the end of a simulation step (after execution of all agents).
	\item[ConstantStepTimeManager] increments time by the duration of one step, after all agents were executed.
	\end{description}
	\begin{center}
		\includegraphics[width=\linewidth]{tinymas_timemanager}
	\end{center}
	\putat(-1,23){\includeanimatedfigure[width=\linewidth]{timemanager_dp}}
\end{frame}

\begin{frame}<5->{Time for the Agent}
	\alertbox{Agents must not access to the time manager directly. \\ (for avoiding a call to the setters)}
	\alertbox*{Application of design patterns}
	\begin{center}
		\includegraphics[width=\linewidth]{tinymas_timemanager}
	\end{center}
	\putat(-1,24){\includeanimatedfigure[width=\linewidth]{timemanager_dp}}
\end{frame}

\subsection{Agent Repository}

\begin{frame}{Repository of Agents}
	\footnotesize
	\alertbox{How to store the identifier-agent mapping?}	
	\begin{description}
	\item Create a map implementation with a specific class interface: the ``white pages''.
	\item[Properties] \begin{itemize}
		\item Contains \Emph{all the agents} that are living on the current platform kernel.
		\item Store information for retreiving the agents located on remote kernels.
		\end{itemize}
	\end{description}
	\begin{center}
		\includegraphics[width=.9\linewidth]{tinymas_whitepages}
	\end{center}
\end{frame}

\subsection{Service Repository}

\begin{frame}{Service: What is it?}
	\begin{definition}
		A service is something that an agent can do, and another agent could query for using this service.
	\end{definition}
	\vspace{2em}
	\alertbox{How to store the mapping between a service and an agent?}
\end{frame}

\begin{frame}{Repository of Services}
	\footnotesize
	\alertbox{How to store the service-agent mapping?}	
	\begin{description}
	\item Create a map implementation with a specific class interface: the ``yellow pages''.
	\item[Values] the names of the services.
	\item[Keys] a collection of the identifiers of the agents that are able to provide the service. 
	\end{description}
	\begin{center}
		\includegraphics[width=\linewidth]{tinymas_yellowpages}
	\end{center}
\end{frame}

\subsection{Message-based Communication}

\begin{frame}{{What is a Direct Interaction} between Agents?}
	\small
	\begin{definition}[Direct interaction]
		A direct interaction between agents permits to the agents exchanging a piece of information.
		A direction interaction may take different forms:
		\begin{description}
		\item[Message] may contain complex information. The sender knows the receiver(s). The receivers know the emitter.
		\item[Event] may contain complex information. The sender doesn't know the receiver(s). The receivers know the emitter.
		\item[Signal] contains basics information (number, etc.) The sender doesn't know the receiver(s). The receivers know the emitter.
		\end{description}
	\end{definition}
	\vspace{1em}
	\begin{center}
		\includegraphics[width=.5\linewidth]{messages}
	\end{center}
\end{frame}

\animatedfigureslide<2>[scale=.7]{FIPA Messages}{fipa_message}

\animatedfigureslide{Routing Messages}{message_routing}

\animatedfigureslide<3>[scale=.7]{Envelope of the Messages}{fipa_message}

\begin{frame}{Modes of Sending}
	Different modes are usually available for sending a message:
	\begin{description}
	\vfill
	\item[one-to-one] One receiver. The emitter specifies the address of the single receiver.
	\vfill
	\item[explicit one-to-many] Multiple receivers. The emitter specifies the addresses of the receivers.
	\vfill
	\item[implicit one-to-many] (or broadcast) Multiple receivers. The emitter does not specify the addresses of the receivers.
	\end{description}
\end{frame}

\begin{frame}{Consuming the Messages}
	\footnotesize
	\begin{block}{Mailbox Approach {\smaller(e.g. TinyMAS)}}
		\begin{itemize}
		\item The messages are put inside the \Emph{mailbox} of the receiving agent.
		\item The receiving agent explicitly takes a message in its behavior.
		\item Usually, the messages are sorted in the mailbox (fifo, timestamp, etc.)
		\item Accesses to the mailbox:
			\begin{itemize}
			\item read [and consume] the first available message.
			\item read [and consume] the first message that is matching a condition.
			\end{itemize}
		\end{itemize}
	\end{block}
	\begin{block}{Event/Signal Approach {\smaller(e.g. Janus)}}
		\begin{itemize}
		\item The events (or signals) are directly fired into the context of the receiving agent.
		\item The agent cannot specify the event taking in its behavior. It can only specify event handlers.
		\item Usually, events are fifo.
		\end{itemize}
	\end{block}
\end{frame}

\figureslide[subtitle={Kernel Store}]{Communication API in TinyMAS}{tinymas_mts1}

\figureslide[subtitle={Agent Store}]{Another Communication API}{tinymas_mts2}

\subsection{Agent}

\begin{frame}{Agent in a Platform}
	\footnotesize
	\begin{block}{Key points for the design}
		\begin{itemize}
		\item Agent is an abstract class.
		\item \alert{Subclasses (defined in applications) must not access to the kernel's services directly.} \\
			$\Rightarrow$ the Agent class provides protected functions for accessible operations.
		\item Agent must exhibits the functions related to the agents' lifecycle.
		\end{itemize}
	\end{block}
	\begin{columns}
		\begin{column}{.3\linewidth}
			\centering
			\includegraphics[height=.54\textheight]{tinymas_lifecycle}
		\end{column}
		\begin{column}{.7\linewidth}
			\includegraphics[width=\linewidth]{tinymas_agent}
		\end{column}
	\end{columns}
\end{frame}

\subsection{Agent Execution}

\begin{frame}{{Agent Execution:} what is it?}
	\scriptsize
	\begin{definition}[Agent Execution]
		\begin{itemize}
		\item Agents are distributed entities.
		\item From the agent point-of-view, they are executed in parallel \citep{Resnick95}.
		\item The activity diagram below is executed by every agent, in parallel.
		\end{itemize}
	\end{definition}
	\centering\includegraphics[height=.6\textheight]{tinymas_living}
\end{frame}

\begin{frame}{Usual Execution Problems}
	\begin{alertblock}{Problem 1}
		How to ensure that messages are inside the mailbox at the right time? \\
		(similar problem for the perception of the environment)
	\end{alertblock}
	\begin{alertblock}{Problem 1}
		How to ensure that the agent messages that must be sent at time $t$ are all sent at this time? \\
		(similar problem for the actions in the environment)
	\end{alertblock}
	\begin{block}{Solution}
		Synchronization of the agents according on the input information.
	\end{block}
\end{frame}

\begin{frame}{Synchronization}
	\begin{definition}[\citep{Filloque92}]
	The treatment of an event must neither be early nor late.
	\end{definition}
	\begin{itemize}
	\vfill
	\item To ensure this assumption, synchronization mechanisms must be used.
	\vfill
	\item To be synchronized, you must ensure that the two following constraints are always true:
		\begin{enumerate}
		\vfill
		\item Causality.
		\vfill
		\item Liveliness.
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame}[t]{Causality}
	\footnotesize
	\begin{definition}[\citep{Chandy88}]
	The \emph{causality} is the principle of no influence of the future on the past. If this constraint is respected locally and that interactions are only done with stamped messages, then the simulation is processing the messages in an order consistent with the partial orders imposed by the constraint of causality of the real system.
	\end{definition}
	\begin{center}
		\includeanimatedfigure[height=.6\textheight]{causality}
	\end{center}
\end{frame}

\begin{frame}{Liveliness}
	\footnotesize
	\begin{definition}[\citep{Chandy88}]
	The \emph{liveliness} is the constraint, which is requiring the time to always
evolve. Its respect allows a simulation not being in deadlock.
	\end{definition}
\end{frame}

\begin{frame}{{Parallel Execution} of Agents}
	\alertbox{How to reproduce the parallel execution of the agents on a computer?}
	\begin{block}{Typical Approaches}
		\begin{itemize}
		\item Run agents on a computer network, or a grid computer.
		\item Associate one thread per agent.
		\item Run synchronously the agents inside a loop, aka. \Emph{scheduling of the execution} of the agents.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Execution Scheduling}
	\begin{definition}
		The execution scheduling imposes a sequential execution of the agents having only a notion of distributed or parallel execution.
	\end{definition}
	\vspace{1em}
	\begin{itemize}
	\item When simulating with a multi-agent system, it is assumed that the actions of the agents are synchronized \citep{Michel.07}.
	\item The execution of the agents (order, etc.) has a strong impact on the results provided by the simulation \citep{Lawson00}.
	\end{itemize}
\end{frame}

\begin{frame}{Loop-based Scheduling}
	\begin{definition}
		\begin{itemize}
		\item A loop-based scheduling is a discrete execution policy that executes the agents inside a virtual loop.
		\item At each loop step, the time evolves with a predefined amount.
		\end{itemize}
	\end{definition}
	\vspace{.5em}
	\begin{block}{Two approaches in this Lecture}
		\begin{enumerate}
		\item Basic constant-step scheduling.
		\item Buffering constant-step scheduling.
		\end{enumerate}
	\end{block}
\end{frame}

\begin{frame}{{Basic Constant-Step} Scheduling}
	\begin{block}{Principle}
		\begin{enumerate}
		\item Sequential activation of all the agents:
			\begin{itemize}
			\item Perception and actions are done when they are requested.
			\end{itemize} 
		\item Increment the time by a predefined amount.
		\end{enumerate}
	\end{block}
	\vspace{2em}
	\begin{columns}
		\begin{column}{.4\linewidth}
			\includegraphics[width=\linewidth]{tinymas_csloop}
		\end{column}
		\begin{column}{.4\linewidth}
			\begin{algorithmic}\smaller\smaller
			\State $t \gets 0$
			\Loop
				\ForAll{$a \in AGENTS$}
					\State $\Call{live}{a}$
				\EndFor
				\State $t \gets t + \Delta t$
			\EndLoop
			\end{algorithmic}
		\end{column}
	\end{columns}
\end{frame}

\figureslide{Timelines with the Basic Constant-Step Scheduling}{csloop_timelines}

\begin{frame}[t]{Problem of the Execution Order}
	\small
	\begin{itemize}
	\item Let a prey try to escape predators.
	\item Let predators try to catch the prey.
	\item Let the environment be a 3x3 grid.
	\item Agents are inserted inside the execution list at the given indexes.
	\item In the model, $t$ is the same for all the agents $\Rightarrow$ they perceive and act at the same time.
	\end{itemize}
	\begin{center}
		\includegraphics[width=.8\linewidth]{execution_order_problem}
	\end{center}
\end{frame}

\sidenote{Influence-Reaction Model \citep{Michel.07}}
\begin{frame}[t]{Buffering Constant-Step Scheduling}
	\begin{footnotesize}
	\begin{block}{Principle}
		\begin{enumerate}
		\item Sequential activation of all the agents:
			\begin{itemize}
			\item Perceptions are on the same environment state for all agents.
			\item Actions are gathered, but never directly applied.
			\end{itemize}
		\item Conflicts among agent's actions are detected and solved.
		\item Corresponding reactions are applied on the environment.
		\item Increment the time by a predefined amount.
		\end{enumerate}
	\end{block}
	\end{footnotesize}
	\begin{columns}
		\begin{column}{.3\linewidth}
			\includegraphics[width=\linewidth]{tinymas_bcsloop}
		\end{column}
		\begin{column}{.5\linewidth}
			\begin{algorithmic}\smaller\smaller\smaller
			\State $e \gets \Call{new}{Environment}$
			\State $t \gets 0$
			\Loop
				\State $actions \gets \Call{new}{List}$
				\ForAll{$a \in AGENTS$}
					\State $actions \gets actions \cup \Call{live}{a}$
				\EndFor
				\State $reactions \gets \Call{resolveConflicts}{e, actions}$
				\ForAll{$a \in reactions$}
					\State $\Call{apply}{e, a}$
				\EndFor
				\State $t \gets t + \Delta t$
			\EndLoop
			\end{algorithmic}
		\end{column}
	\end{columns}
\end{frame}

\figureslide{Timelines with Buffering Constant-Step Scheduling}{bcsloop_timelines}

\begin{frame}[t]{Message-based Synchronization}
	\begin{footnotesize}
	\begin{block}{Basic Principle}
		\begin{enumerate}
		\item Determine the messages with the minimal timestamp.
		\item Test if the causility is broken.
		\item Set $t$ to the minimal timestamp.
		\item Activate the agents.
		\end{enumerate}
	\end{block}
	\end{footnotesize}
	\begin{columns}
		\begin{column}{.25\linewidth}
			\includegraphics[width=\linewidth]{tinymas_msgloop}
		\end{column}
		\begin{column}{.5\linewidth}
			\begin{algorithmic}\smaller\smaller\smaller
			\State $e \gets \Call{new}{Environment}$
			\State $mb \gets \Call{new}{MailboxManager}$
			\State $t \gets 0$
			\Repeat
				\State $(m_t, msgs) \gets \Call{getMin}{mb}$
				\State $\Call{assert}{m_t > t}$
				\State $t \gets m_t$
				\State $\Call{updateEnvironment}{e, msgs}$
				\ForAll{$m \in msgs$}
					\ForAll{$a \in AGENTS$}
						\If{$\Call{receiver}{m} = a$}
							\State $\Call{live}{a, m}$
						\EndIf
					\EndFor
				\EndFor
			\Until{$mb = \emptyset$}
			\end{algorithmic}
		\end{column}
	\end{columns}
	\putat(150,72.5){\includeanimatedfigure[width=.6\linewidth]{msgloop_assert}}
\end{frame}

\begin{frame}{Approaches for Message-based Synchronisation}
	\begin{description}
	\item[Pessimistic approach] time progresses for all the agents at the same time. Fast agents must wait for slow agents.
	\vfill
	\item[Optimistic approach] time progresses for each agent as fast as possible, and the system go back in time in case of causality problem.
	\vfill
	\item[Hybrid approach] mixing the two previous approaches.
	\end{description}
\end{frame}

\begin{frame}[t]{Pessimistic Message-based Synchronisation}
	\begin{definition}[\citep{Chandy79}, adapted to agent-based systems]
	An agent must treat a message only if it is sure that it will never receive any message with a timestamp lower than or equal to its current agent time.
	\end{definition}
	\vspace{.5em}
	\begin{description}
	\item[Causality] It is never broken.
	\end{description}
	\begin{center}
		\includegraphics[width=.6\linewidth]{pessimistic}
	\end{center}
	\begin{description}
	\item[Liveliness] Must be managed by hand.
	\end{description}
\end{frame}

\animatedfigureslide{Example of the Liveliness Problem}{liveliness_problem}

\begin{frame}{{NULL Messages} for Preserving Liveliness}
	\begin{itemize}
	\item NULL message is an empty message with a timestamp \citep{Chandy79}.
	\vspace{2em}
	\item NULL messages are sent to the connected agents when no other type of message is available.
	\end{itemize}
\end{frame}

\animatedfigureslide{Example of the NULL Messages}{null_messages}

\begin{frame}[t]{Optimistic Message-based Synchronisation}
	\small
	\begin{definition}[\citep{Fujimoto90}, adapted to agent-based systems]
	Optimistic approach does not impose any constraint on the simulation model. It is running the agents as fast as possible until a causality problem is detected. Then the simulator goes back in time to the last valid state of the simulator.
	\end{definition}
	\vspace{.5em}
	\begin{description}
	\item[Liveliness] It is never broken.
	\end{description}
	\begin{center}
		\includegraphics[width=.5\linewidth]{optimistic}
	\end{center}
	\begin{description}
	\item[Causality] It is broken when a message received by an agent has a timestamp lower than or equal to the agent's time.
	\end{description}
\end{frame}

\begin{frame}{{What Doing} When Causality is Broken?}
	\begin{block}{Solution}
		\begin{itemize}
		\item The last correct state is detected from the history of changes.
		\item The state of the agent is restored to this last correct state.
		\item All sent messages must be cancelled!
		\item The state of the environment is restored.
		\end{itemize}
	\end{block}
	\vspace{1em}
	\begin{block}{Assumptions}
		\begin{itemize}
		\item The time spent to go back is negligible.
		\item The simulator has enough memory to store all the states.
		\end{itemize}
	\end{block}
\end{frame}

\figureslide{Steps of the Optimitic Approach}{tinymas_msgloop2}

\begin{frame}{Hybrid Message-based Synchronisation}
	\begin{block}{\citep{Reynolds88}}
	Purely pessimistic or optimistic approaches are not suitable in real case studies. \\
	Hybrid approaches use both previous approaches at the same time.
	\end{block}
	\vspace{.5em}
	\begin{example}[1]
	Safety indicators are computed for each message. Safe messages do not stop the simulator as for the optimistic approach. Unsafe messages are blocking the simulator as for the standard pessimistic approach.
	\end{example}
	\begin{example}[2]
	The simulator may use an optimistic approach until it reaches a rendez-vous time.
	\end{example}
\end{frame}

\subsection{Probing the Agents}

\begin{frame}{Probes on Agents}
	\footnotesize
	\begin{definition}[State of an Agent]
		Each agent is composed of three major elements:
		\begin{description}
		\item[Identity or address] The property of an agent that distinguishes it from other agents.
		\item[Attributes] It describes the data stored in the agents, aka. the attributes.
		\item[Behavior] It describes the behavior of the agent (its functions activate, live and end, and all the functions called by one of them).
		\end{description}
		The \emph{state of an agent} is close to the concept of state of an object in object-oriented computer programming. It is built with the \emph{identity} and the \emph{attributes}.
	\end{definition}
	\begin{definition}[Probe]
		\begin{itemize}
		\item A \emph{probe} is a tool that permits to retreive the state of an agent.
		\item A probe must be \emph{non-invasive for the agent}. It means that the agent cannot determine when a probe is getting a value from its state. Moreover, an agent is not able to obtain the list of the probes on it, easily.
		\end{itemize}
	\end{definition}
\end{frame}

\begin{frame}[fragile]{Probe Implementation}
	\alertbox*{The reflection mechanism of the Java runtime environment will enable you to retreive the attribute values in a non-invasive way.}
	\vspace{1em}
	\begin{columns}
		\begin{column}{.7\linewidth}
			\begin{lstlisting}
class ProbeManager{
   def getProbeValue(valueType : Class<T>,
                     ^agent : Agent, name : String)
       : T with T {

      var type = ^agent.getClass
      while (typeof(Agent) != type) {

         try {
            return valueType.cast(
               type.getDeclaredField(name)
                   .get(^agent))
         } catch (exception : Throwable){}

         type = type.superclass

      }

      throw new NoSuchFieldException(name)
   }
}
			\end{lstlisting}
		\end{column}
		\begin{column}{.3\linewidth}
			\includegraphics[width=\linewidth]{java_reflect}
		\end{column}
	\end{columns}
\end{frame}

\subsection{Kernel}

\begin{frame}[t]{Kernel of the Agent Platform}
	\begin{itemize}
	\item The kernel is the central class of the platform.
	\item It contains all the internal and external services of the platform.
	\item It provides to the agents an interface accessing the platform services.
	\end{itemize}
	\vspace{-1em}
	\begin{center}
		\includegraphics[width=.8\linewidth]{tinymas_kernel}
	\end{center}
\end{frame}

\section[Service-based Platform]{Service-based Platform: the example of the Janus platform}

\begin{frame}[t]{Janus Platform}
	\small
	\wrapfigure[width=.15\linewidth]{janus}
	\begin{itemize}
	\wrapitem[.75\linewidth]{Designed for a large set of application domains (academic, research, industrial).}
	\wrapitem[.75\linewidth]{It supports the execution of agents written with the \emph{SARL language}.}
	\wrapitem[.75\linewidth]{Use a \emph{fully parallel execution}, via extensive use of thread pools.}
	\item Native and transparent support of \emph{computer networks}.
	\item Easy to plug/unplug kernel instances from a collection of connected computers.
	\end{itemize}
	\vfill
	\begin{center}
		Official website: \\
		\url{http://www.janusproject.io/} \\[1em]
		Source code: \\
		\url{https://github.com/janus-project/janusproject/}
	\end{center}
\end{frame}

\begin{frame}[t]{{General Architecture} of Janus}
	\begin{center}
		\includeanimatedfigure[height=.8\textheight]{janus_architecture}
	\end{center}
	\putat<3>(70,137){\includegraphics[width=.6\linewidth]{janusuml_network}}
	\putat<4>(70,137){\includegraphics[width=.6\linewidth]{janusuml_servicedeps}}
\end{frame}

\section{Conclusion}

\begin{frame}{{Key Elements} for this Chapter}
	\begin{itemize}
	\item The \Emph{key features} of an agent platform are related to technological, domain, development, analysis, and exploration needs.
	\vspace{.25em}
	\item The platform provides the features for the \Emph{targeted users}:
		\begin{description}
		\item[Beginners] a nice UI for creating the agents, e.g. NetLogo.
		\item[Experts] a powerfull programming API, e.g. Janus.
		\item[All] the lower-level software/hardware issues must be hidden to agent developers.
		\end{description}
	\vspace{.25em}
	\item A ``good'' platform provides \Emph{distributed execution} via threads, computer networking, or both.
	\vspace{.25em}
	\item The \Emph{typical architecture} is usually service oriented.
	\vspace{.25em}
	\item The platform should be coded with \Emph{design patterns} for making easier its code upgrades.
	\vspace{.25em}
	\item It must be \Emph{efficient} for almost all the target users.
	\end{itemize}
\end{frame}

\end{graphicspathcontext}

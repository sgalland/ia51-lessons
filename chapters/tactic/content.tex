\part[author={\protect\insertauthor}]{Tactical Behaviors: where agents should go}\label{chap:tactic}

\begin{graphicspathcontext}{{./chapters/tactic/imgs/},{./chapters/tactic/imgs/auto/},{./chapters/intro/2mabs/imgs/auto/},\old}

\begin{frame}{Goals of this Lecture}
	During this lecture, I will present:
	\begin{enumerate}
	\item the basics of the coordinate motion of groups of agents.
	\item the concept of formation with or without leader.
	\item the basics of the swarm intelligence related coordinated motion.
	\vspace{1em}
	\item the basics path finding approaches.
	\end{enumerate}
\end{frame}

\figureslide{{Tactic decisions} in the Simulator Architecture}{mabs_tactic_focus}

\tableofcontentslide

\section[Coordinated Motion]{Coordinated Motion}

\subsection{Notion of Coordinated Motion}
\subsectiontableofcontentslide

\begin{frame}{Coordinated Motion}
	\alertbox*{Simulations may require groups of agents to move in a coordinated manner.}
	\vspace{1em}
	\begin{columns}
		\begin{column}[t]{.6\linewidth}
			\begin{itemize}
			\item Typical examples:
				\begin{enumerate}
				\item military squad,
				\item formation of military vehicles,
				\item formation of tractors,
				\item platoon of vehicles.
				\end{enumerate}
			\end{itemize}
		\end{column}
		\begin{column}[t]{.4\linewidth}
			\raisebox{-\height}{\includegraphics[width=\linewidth]{groups}}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{{Two Approaches} for Coordinated Motion}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{block}{Centralized}
				\begin{itemize}
				\item \emph{Centralized and shared formation definition.}
				\item Prescribed formation.
				\item Agents may acts as leader or member.
				\item Formation definition could dynamically evolve.
				\end{itemize}
			\end{block}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{block}{Decentralized}
				\begin{itemize}
				\item \emph{No shared formation definition.}
				\item Emergent formation.
				\item No explicit leader.
				\item Related to swarm intelligence.
				\end{itemize}
			\end{block}
		\end{column}
	\end{columns}
\end{frame}

\figureslide{Examples of Coordinated Motion Problems}{coordinated_motion_typology}

\subsection{Prescribed Movement Formations}
\subsectiontableofcontentslide

\begin{frame}{Movement Formation}
	\smaller
	\alertbox*{A movement formation defines spatial positions and the geometrical contrains among these positions.}
	\vspace{1em}
	\begin{columns}
		\begin{column}{.6\linewidth}
			\begin{description}
			\item[Anchors] define the location where an agent should be, and its expected orientation.
				\begin{itemize}
				\item \Emph{relative position} to the formation's reference position or another anchor.
				\item \Emph{relative orientation} to the formation's reference orientation or another anchor.
				\item the type of the entity to be located in the anchor.
				\end{itemize}
			\item[Geometrical Constraints] defined by the relative positions of the anchors.
			\end{description}
		\end{column}
		\begin{column}{.4\linewidth}
			\includegraphics[width=\linewidth]{tacticuml_formation}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Examples of Formations}
	\begin{columns}
		\begin{column}{.3\linewidth}
			\begin{itemize}
			\item<1> Line
			\item<2> Chevron
			\item<3> Defensive circle
			\end{itemize}
		\end{column}
		\begin{column}{.7\linewidth}
			\begin{center}
				\only<1>{
					\includegraphics[width=.75\linewidth]{lineformation_example} \\[2em]
					\includegraphics[width=\linewidth]{tacticuml_lineformation_example}
				}
				\only<2>{
					\includegraphics[width=.6\linewidth]{chevronformation_example} \\[2em]
					\includegraphics[width=\linewidth]{tacticuml_chevronformation_example}
				}
				\only<3>{
					\includegraphics[width=.35\linewidth]{defensivecircleformation_example} \\[2em]
					\includegraphics[width=\linewidth]{tacticuml_defensivecircleformation_example}
				}
			\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Scalable Movement Formation}
	\smaller
	\alertbox{In many situations, the exact structure of a formation will depend on the \emph{number of agents} participating in it.}
	\vspace{1em}
	\alertbox*{Scalable formations without an explicit list of anchor positions and orientations}.
	\begin{center}
		\includegraphics[width=.65\linewidth]{scalableformations}
	\end{center}
\end{frame}

\begin{frame}[t,fragile]{Factory of Movement Formations}
	\begin{columns}
		\begin{column}[t]{.7\linewidth}
			\vspace{-1.5em}
			\begin{lstlisting}[basicstyle=\usebeamertemplate{code basic style}\tiny]
class CircleFormationFactory
      extends FormationFactory {

  val defaultRadius : float
  val interSpace : float

  new (radius : float = 20f,
       interSpace : float = 10f) {
    this.defaultRadius = radius
    this.interSpace = interSpace
  }

  def newFormation(numberAnchors : int):Formation{
    var f = new Formation
    var radius = (this.defaultRadius * 2 +
                 this.interSpace) * numberAnchors
                 / (2*Math::PI)
    var delta = Math::PI * 2 / numberAnchors
    for (i : 0 ..< numberAnchors) {
      var angle = (i + 0.5) * delta
      var v = Vector2f::toOrientationVector(angle)
      v *= radius
      var a = new FormationAnchor => [
        relativePosition = v
        relativeOrientation = angle
        parentAnchor = null
      ]
      f.addAnchor(a)
    }
  }
}
			\end{lstlisting}
		\end{column}
		\begin{column}[t]{.4\linewidth}
			\raisebox{-\height}{\includegraphics[width=\linewidth]{tacticuml_formationfactory}}
		\end{column}
	\end{columns}
	\putat(10,33){\includeanimatedfigure[width=.9\linewidth]{circleformationfactory}}
\end{frame}

\begin{frame}{Formation of Formations}
	\alertbox*{Formation concept can be extended to more levels, giving the ability to create \emph{formations of formations}.}
	\begin{columns}
		\begin{column}{.7\linewidth}
			\begin{itemize}
			\item Each formation has its own steering anchor point into a ``super'' formation.
			\vspace{1em}
			\item When the super formation anchor moves, the sub-formation moves also.
			\vspace{1em}
			\item \inlineexamples{Squad and platoon formations.}
			\end{itemize}
		\end{column}
		\begin{column}{.3\linewidth}
			\includegraphics{tacticuml_formationformation}
		\end{column}
	\end{columns}
	\begin{center}
		\includegraphics[width=.4\linewidth]{formationformationimg}
	\end{center}
\end{frame}

\begin{frame}{Agent \& Formation}
	\begin{itemize}
	\item Each agent has access to the formation definition.
	\vspace{1em}
	\item Two main behaviors are possible:
		\begin{enumerate}
		\item[Member] steers to the formation anchor that was selected by or assigned to it.
		\item[Leader] changes the position and the orientation of the formation.
		\end{enumerate}
	\end{itemize}
	\begin{center}
		\includegraphics[width=.5\linewidth]{tacticuml_agentformation}
	\end{center}
\end{frame}

\begin{frame}[t,fragile]{Example of a Member Agent}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}[basicstyle=\usebeamertemplate{code basic style}\tiny]
agent MemberExample {

  /* Reference to the formation. */
  var formation : Formation

  /* Index of the slot that is targeted
     by this agent. */
  var formationAnchorIndex : int

  /* Arriving behavior. */
  var arriving : ArrivingBehavior

  /* Aligning behavior. */
  var aligning : AligningBehavior

  /* Behavior for avoiding
     collisions. */
  var avoidingCollision
      : ForceBaseBehavior

  on PerceptionEvent {
    var b = occurrence.body

    /* Get the anchor */
    var anchor = this.formation
          .getAnchorAtIndex(
            formationAnchorIndex)
			\end{lstlisting}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}[basicstyle=\usebeamertemplate{code basic style}\tiny]
    /* Arriving on the anchor */
    var o1 = this.arriving.run(
       anchor.position,
       b.position,
       b.maxLinearAcceleration,
       b.maxLinearSpeed,
       b.currentLinearMotion)

    /* Aligning to the anchor */
    var o2 = this.aligning.run(
       anchor.orientation,
       b.direction,
       b.maxAngularAcceleration,
       b.maxAngularSpeed,
       b.currentAngularMotion)

    /* Avoiding collisions */
    var o3 = this.avoidingCollision
             .run(occurrence.objects)

    /* Arbitration */
    if (! o3.linearMotion.empty) {
      return o3 + o2
    }
    return o1 + o2
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile]{Example of a Leading Agent}
	\begin{lstlisting}
agent LeadingExample {

  /* Reference to the formation. */
  var formation : Formation

  /* Some movement behavior. */
  var someBehavior : Behavior

  on PerceptionEvent {
    var b = occurrence.body

    /* The leader behaves by executing its movement behavior. */
    var o = this.someBehavior.run

    /* Change the position of the formation. */
    this.formation.position = b.position

    /* Change the orientation of the formation. */
    this.formation.orientation = b.direction

    return o

  }

}
	\end{lstlisting}
\end{frame}

\begin{frame}{{How Sharing} the Formation?}
	\begin{block}{Centralized Formation}
		\begin{itemize}
		\item \code{Formation} object is a centralized and shared object.
		\item It may be implemented with:
			\begin{itemize}
			\item an attribute with a shared object (see previous examples) \\
				$\Rightarrow$ it avoids distribution over VMs.
			\item formation is an environment artifact \\
				$\Rightarrow$ it is safe, but an environment is needed.
			\end{itemize}
		\end{itemize}
	\end{block}
	\begin{block}{Decentralized Formation}
		\begin{itemize}
		\item Each agent has a copy of the \code{Formation} object.
		\item Leading agent sends updates to the formation members.
		\item Each member updates its knowledge, and uses it.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Demo of Prescribed Formation}
	\begin{center}
	\includegraphics[width=.6\linewidth]{formationlabwork} \\[1em]
	\smaller\smaller On-Computer Tutorial Session \#3 \\
	\url{https://github.com/gallandarakhneorg/vi51-student-sarl-simulation}
	\end{center}
\end{frame}

\subsection{Emergent Formation}
\subsectiontableofcontentslide

\begin{frame}{{Distributed Coordination} in Nature}
	\begin{columns}
		\begin{column}{.7\linewidth}
			\begin{itemize}
			\item Flocks, swarms and schools exhibit coordinated group behavior although each animal acts completely autonomously.
			\vspace{1em}
			\item How do these behaviors emerge?
			\item How are they sustained?
			\item How do individual decisions lead to collective group behavior?
			\end{itemize}
		\end{column}
		\begin{column}{.3\linewidth}
			\includegraphics{flockofbirds} \\
			\includegraphics{schooloffishes}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Emergent Formation}
	\begin{itemize}
	\item \Emph{There is no overall formation geometry in this approach.}
	\vspace{1em}
	\item Formation emerges from individual rules of each agent. \\
		\inlineexample{flocking behaviours emerge from the steering behaviour of each flock member.}
	\vspace{1em}
	\item Group does not necessarily have a leader.
	\vspace{1em}
	\item Emergent formations provide a different solution to scalability.
	\vspace{1em}
	\end{itemize}
	\alertbox*{No need to adapt the formation according to the size, because each individual in the formation will act appropriately.}
\end{frame}

\sidecite{Reynolds.99}
\begin{frame}{{Flocking Behavior:} the Boids}
	\smaller
	\alertbox{How to reproduce flocking behavior (birds or fishes)?}
	\begin{itemize}
	\item Boids = ``bird-oids'' \citep{Reynolds.99}
	\item Force-based motion behavior, with three forces: \begin{enumerate}
		\item[separation] avoid collision with neighbor birds (repulsive force).
		\item[cohesion] be attracted by the barycenter of the group (attractive force).
		\item[alignment] have the same general direction as the group (attractive force).
		\end{enumerate}
	\end{itemize}
	\begin{center}
		\begin{tabular}{c@{\hspace{2em}}c@{\hspace{2em}}c@{\hspace{2em}}c@{\hspace{2em}}c}
			\includegraphicswtex[width=.15\linewidth]{boids_separation} &
			{\Huge $+$} &
			\includegraphicswtex[width=.15\linewidth]{boids_alignment} &
			{\Huge $+$} &
			\includegraphicswtex[width=.15\linewidth]{boids_cohesion} \\
			\small separation &	&
			\small alignment & &
			\small cohesion \\
		\end{tabular}
	\end{center}
\end{frame}

\sidecite{Reynolds.99}
\begin{frame}{Arbitrating Behaviors}
	\begin{itemize}
	\item Behavioral urges produce acceleration requests: normalized 3D vector with importance in $[0,1]$.
	\item Priority acceleration allocation is used instead of averaging acceleration requests.
	\item Acceleration requests are prioritized and the most important ones are used up to a maximum acceleration.
	\end{itemize}
	\begin{center}
		\begin{tabular}{c@{\hspace{2em}}c@{\hspace{2em}}c@{\hspace{2em}}c@{\hspace{2em}}c}
			\includegraphicswtex[width=.15\linewidth]{boids_separation} &
			{\Huge $+$} &
			\includegraphicswtex[width=.15\linewidth]{boids_alignment} &
			{\Huge $+$} &
			\includegraphicswtex[width=.15\linewidth]{boids_cohesion} \\
			\small separation &	&
			\small alignment & &
			\small cohesion \\
		\end{tabular}
	\end{center}
\end{frame}

\sidecite{Reynolds1987,Tu1994}
\begin{frame}{Schooling Behaviors}
	\begin{itemize}
	\item For schooling behavior, we need to consider the following behaviors:
		\begin{enumerate}
		\item Eating
		\item Avoiding predators
		\item Mating
		\end{enumerate}
	\vspace{1em}
	\item Modeled with limited:
		\begin{itemize}
		\item memory, in terms of number of attributes and time validity of the values;
		\item perception of the world,
		\item steering behavior (see Chapter \ref{chap:bodymotion}); and
		\item physics, for modeling the body of the fish (see Chapter \ref{chap:environment}).
		\end{itemize}
	\end{itemize}
\end{frame}

\sidecite{Reynolds1987,Tu1994}
\begin{frame}{Realistic Sensors}
	\begin{itemize}
	\item \alert{Unrealistic for each boid to have complete knowledge.}
	\vspace{1em}
	\item Flocking depends upon a localized view of the world.
	\vspace{1em}
	\item Each boid has a spherical neighborhood of sensitivity, based upon a radius and an exponent: $\frac{1}{r^n}$.
	\vspace{1em}
	\item Vision covers 300 degree spherical angle extending to some effective radius based on water translucency.
	\vspace{1em}
	\item Temperature sensor samples ambient water temperature at center of fish's body.
	\end{itemize}
\end{frame}

\sidecite{Reynolds1987,Tu1994}
\begin{frame}[t]{{Mental State} of a Fish}
	\begin{description}
	\item[Hunger] $H(t) = \min\left[ \frac{1 - n^e(t) R(\Delta t^H)}{\rho}, 1\right]$, where: \begin{itemize}
			\item $n^e(t)$: amount of food consumed;
			\item $R(x) = 1 – p_0x$, $p_0$ is the digestion rate ($0.005$ for ravenous fish);
			\item $\Delta t^H$: time since last meal;
			\item $\rho$: appetite.
			\end{itemize}
	\vspace{1em}
	\item[Libido] $L(t) = \min\left[ s(\Delta t^L) (1-H(t)), 1\right]$, where: \begin{itemize}
			\item $s(x) = p_1x$, $p_1$ is the libido constant ($0.01$ for sexual mania);
			\item $\Delta t^L$: time since last mating.
			\end{itemize}
	\vspace{1em}
	\item[Fear] $F(t) = \min\left(\sum_i \min\left[\frac{\alpha}{d_i(t)}, 1\right], 1\right)$, where: \begin{itemize}
			\item $\alpha = 100$ (constant);
			\item $d_i(t)$: distance to visible predator $i$.
			\end{itemize}
	\end{description}
	\alertbox*{The mental state may be used for determining the priorities in the Boid behavior.}
\end{frame}

\section{Path Finding}

\subsection{Introduction}
\subsectiontableofcontentslide

\begin{frame}{Intoduction}
	\begin{itemize}
	\item For each character, AI may be able to calculate a suitable route through the environment to reach its goal.
	\item This process is called:
		\begin{itemize}
		\item \emph{Path finding} in artificial intelligence, and
		\item \emph{Path planning} in robotics.
		\end{itemize}
	\vspace{1em}
	\item \emph{Major part of simulations use path finding solutions based on the A* algorithm.}
	\end{itemize}
\end{frame}

%\begin{frame}{Graph for Path Finding}
%	\begin{itemize}
%	\item Representation of the Universe topology: \emph{directed non-negative weighted graph}.
%	\end{itemize}
%	\begin{definition}[Graph $G$]
%		$G = \langle N, E \rangle$ \\
%		where: \begin{itemize}
%			\item $N$ is the set of nodes, and
%			\item $E$ is the set of edges, $S: N \rightarrow N$
%			\end{itemize}
%	\end{defintion}
%\end{frame}

%\begin{frame}{Hierarchical Path Finding}
%	%Site thèse Luk => Nb de segments dans une trip sequence est limité à quelques elements (4-6).
%	\citep{Knapen2015}
%\end{frame}

%\subsection{Dijkstra Algorithm}
%\subsectiontableofcontentslide

%\begin{frame}{Dijkstra Algorithm}
%\end{frame}

%\subsection{A* Algorithm}
%\subsectiontableofcontentslide

%\begin{frame}{A* Algorithm}
%\end{frame}

%\begin{frame}{IDA* Algorithm}
%\end{frame}

%\subsection{D* Algorithm}
%\subsectiontableofcontentslide

%\begin{frame}{D* Algorithm}
%\end{frame}

%\subsection{Time-based Path Finding}
%\subsectiontableofcontentslide

%\begin{frame}{Time-based Path Finding}
%\end{frame}

\section{Conclusion}

\begin{frame}{{Key Elements} for this Chapter}
	\begin{description}
	\item[Prescribed Formation] \begin{itemize}
		\item Definition.
		\item Usage in multiagent systems.
		\end{itemize}
	\vspace{1em}
	\item[Emergent Formation] \begin{itemize}
		\item No predefined definition.
		\item Emerged from the agent behaviors and their interactions.
		\end{itemize}
	\vspace{1em}
	\item[Path Finding] A*, D*
	\end{description}
\end{frame}

\end{graphicspathcontext}


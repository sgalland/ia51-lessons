\part[author={\protect\insertauthor}]{Simulation and Multiagent based Simulation}
\label{chap:overview}

\begin{graphicspathcontext}{{./chapters/intro/2mabs/imgs/},{./chapters/intro/2mabs/imgs/auto/},{./chapters/intro/1sarl/imgs/auto/},\old}

\section{Multiagent Simulation}

\subsection{Simulation Fundamentals}

\begin{frame}{Definition of Simulation}
	\begin{definition}[\citep{Shannon77}]
		The process of \alert{designing a model} of a real system and \alert{conducting experiments } with this model for the purpose either of \alert{understanding} the behavior of the system or of \alert{evaluating} various strategies (within the limits imposed by a criterion or a set of criteria) for the operation of the system.
	\end{definition}
	\vfill
	\begin{block}{Why simulate?}
		\begin{itemize}
		\item Understand / optimize a system.
		\item Scenarii/strategies evaluation, testing hypotheses to explain a phenomenom (decision-helping tool).
		\item Predicting the evolution of a system, e.g. metrology.
		\end{itemize}
	\end{block}
\end{frame}

\figureslide[width=.95\linewidth]{Simulation Basics}{simulation_basics}

\sidecite{Zeigler00}
\begin{frame}{{Modeling Relation:} System $\leftrightarrow$ Model}
	\begin{center}
		\includegraphics[width=.8\linewidth]{system_model_relation}
	\end{center}
	\begin{itemize}
	\item To determine if the system model is an acceptable simplificiation in terms of quality criteria and experimentation objectives.
	\item Directly related to the consistency of the model simulation.
	\end{itemize}
\end{frame}

\sidecite{Zeigler00}
\begin{frame}{{Simulation Relation:} Model $\leftrightarrow$ Simulator}
	\begin{center}
		\includegraphics[width=.8\linewidth]{model_simulator_relation}
	\end{center}
	\begin{itemize}
	\item To guarantee that the simulator, used to implement the model, correctly generates the behavior of the model.
	\item To be sure that the simulator reproduces clearly the mechanisms of change of state are formalized in the model.
	\end{itemize}
\end{frame}

\sidecite{Hoogendoorn01, Davidsson00}
\begin{frame}[t]{Classical Typology (1/4)}
	\begin{columns}
		\begin{column}[t]{.8\linewidth}
			\begin{block}{Microscopic Simulation}
				\begin{itemize}
				\item Explicitly attempts to model the behaviors of each individual.
				\item The system structure is viewed as emergent from the interactions between the individuals.
				\end{itemize}
			\end{block}
			\begin{center}
				\includegraphics[height=.35\textheight]{janus_examples}
			\end{center}
		\end{column}
		\begin{column}[t]{.15\linewidth}
			\raisebox{-1.1\height}{\pgfuseimage{simulevels}}
		\end{column}
	\end{columns}
\end{frame}

\sidecite{Hoogendoorn01, Davidsson00}
\begin{frame}[t]{Classical Typology (2/4)}
	\smaller
	\begin{columns}
		\begin{column}[t]{.8\linewidth}
			\begin{block}{Mesoscopic Simulation}
				\begin{itemize}
				\item Based on small groups, within which elements are considered homogeneous.
				\item Examples: vehicle platoon dynamics and household-level travel behavior.
				\end{itemize}
			\end{block}
			\begin{center}
				\includegraphics[height=.35\textheight]{mesosimulation}
			\end{center}
		\end{column}
		\begin{column}[t]{.15\linewidth}
			\raisebox{-1.1\height}{\pgfuseimage{simulevels}}
		\end{column}
	\end{columns}
\end{frame}

\sidecite{Hoogendoorn01, Davidsson00}
\begin{frame}[t]{Classical Typology (3/4)}
	\smaller
	\begin{columns}
		\begin{column}[t]{.8\linewidth}
			\begin{block}{Macroscopic Simulation}
				\begin{itemize}
				\item Based on mathematical models, where the characteristics of a population are averaged together.
				\item Simulate changes in these averaged characteristics for the whole population.
				\item The set of individuals is viewed as a structure that can be characterized by a number of variables.
				\end{itemize}
			\end{block}
			\begin{center}
				\includegraphics[height=.35\textheight]{macrosimulation}
			\end{center}
		\end{column}
		\begin{column}[t]{.15\linewidth}
			\raisebox{-1.1\height}{\pgfuseimage{simulevels}}
		\end{column}
	\end{columns}
\end{frame}

\sidecite{Hoogendoorn01, Davidsson00, GallandGaudDemangeKoukam2014_703}
\begin{frame}[t]{Classical Typology (4/4)}
	\smaller
	\begin{columns}
		\begin{column}[t]{.8\linewidth}
			\begin{block}{Multilevel Simulation}
				\begin{itemize}
				\item Combines various levels.
				\item Specify how the different levels are interacting together: one is input of the other, dynamic selection of the level.
				\end{itemize}
			\end{block}
			\begin{center}
				\includegraphics[height=.35\textheight]{scheduling_holarchy}
			\end{center}
		\end{column}
		\begin{column}[t]{.15\linewidth}
			\raisebox{-1.1\height}{\pgfuseimage{simulevels}}
		\end{column}
	\end{columns}
	\alertbox<2>{Multiagent-based Simulation (MABS), aka. ABS, is traditionnally considered as a special form of microscopic simulation, but not restricted to.}
\end{frame}

\subsection{Multiagent Based Simulation (MABS)}
\begin{frame}{MABS: General Idea}
	\begin{itemize}
	\item Create an artificial world composed of interacting agents.
	\item The behavior of an agent results from:
		\begin{itemize}
		\item its \alert{perceptions/observations};
		\item its internal \alert{motivations/goals/beliefs/desires};
		\item its eventual representations;
		\item its \alert{interaction} with the environment (indirect interactions, ressources) and the other agents (communications, direct interactions, stimuli).
		\end{itemize}
	\item Agents act and modify the state of the environment through their actions.
	\item We observe the results of the interactions like in a Virtual Lab $\Rightarrow$ Emergence.
	\end{itemize}
\end{frame}

\begin{frame}[t]{MABS: Main Characteristics and Advantages}
	\wrapfigure[width=.15\linewidth]{pro-icon}
	\begin{itemize}
	\wrapitem[.75\linewidth]{\emph{More flexible than macroscopic models} to simulate spatial and evolutionary phenomena.}
	\vfill
	\wrapitem[.75\linewidth]{Dealing with real multiagent systems directly: \\ real Agent $=$ simulated Agent.}
	\vfill
	\item Allows modelling of adaptation and evolution.
	\vfill
	\item Heterogeneous space and population.
	\vfill
	\item Multilevel modeling: integrate different levels of observation, and of agent's behaviors.
	\end{itemize}
\end{frame}

\begin{frame}[t]{MABS: Limitations and Drawbacks}
	\wrapfigure[width=.15\linewidth]{cons-icon}
	\begin{itemize}
	\wrapitem[.75\linewidth]{Offer a significant level of accuracy at the expense of a larger computational cost.}
	\vfill
	\wrapitem[.75\linewidth]{Require many and accurate data for their initialization.}
	\vfill
	\item It is difficult to apply to large scale systems.
	\vfill
	\item Actual simulation models are costly in time and effort.
	\end{itemize}
\end{frame}

\subsection{Overview of a MABS Architecture}

\animatedfigureslide{General Architecture}{mabs_overview}

\sidecite{Michel04}
\figureslide{Designing a Multiagent Simulation Model}{model_design}

\subsection{Agent Architectures}

\sidecite{Michon85,Hoogendoorn01,Hoogendoorn04,Montello05}
\begin{frame}{Three-Layer Architecture}
	\begin{columns}
		\begin{column}[c]{.6\linewidth}
			\begin{description}
			\item[Strategic Layer] general planning stage that includes the determination of goals, the route and
the modal choice as well as a cost-risk evaluation.
			\item[Tactic Layer] Maneuvering level behavior. Examples: obstacle avoidance, gap acceptance, turning, and overtaking.
			\item[Operational Layer] Fundamental body controlling processes such as controlling speed, following the path, etc.
			\end{description}
		\end{column}
		\begin{column}[c]{.4\linewidth}
			\raisebox{-\height}{\includegraphics{standard_agent_layers}}
		\end{column}
	\end{columns}
\end{frame}

\sidecite{brooks90}
\begin{frame}[t]{Subsomption Agent Architecture}
	\begin{itemize}
	\item Priority-ordering sequence of condition-action pairs.
	\item Generalization of the three-layer architecture: each pair is a layer. 
	\end{itemize}
	\vspace{.5cm}
	\begin{columns}[t]
		\begin{column}{.39\linewidth}
			\begin{algorithmic}\scriptsize
			\If {$condition_1$}
				\State $action_1$
			\Else
				\If {$condition_2$}
					\State $action_2$
				\Else
					\State \dots
				\EndIf
			\EndIf
			\end{algorithmic}
		\end{column}
		\begin{column}{.59\linewidth}
			\raisebox{-\height}{\includegraphicswtex[width=\linewidth]{subsomption}}
		\end{column}
	\end{columns}
\end{frame}

\sidecite{Helbing.1997,Reynolds.99,buisson.abmtrans13}
\begin{frame}[t]{(Social) Force Models}
	\begin{itemize}
	\item Repulsive forces are computed and summed for obtaining the safer direction.
	\item May contribute to the too lower layers of the three-layer architecture. 
	\end{itemize}
	\vspace{.5cm}
		\[\scriptsize \vec{f} =
		\left( \sum^n_{i=1} \dfrac{\alpha_i.\widehat{p-a_i}}{|\overrightarrow{p-a_i}|} \right)  +
		\beta. \left( \overrightarrow{\dfrac{ \sum^n_{i=1} a_i}{n} - p} \right)  \]
		\begin{center}
			\begin{tabular}{c@{\hspace{2em}}c@{\hspace{2em}}c@{\hspace{2em}}c@{\hspace{2em}}c}
				\includegraphicswtex[width=.15\linewidth]{boids_separation} &
				{\Huge $+$} &
				\includegraphicswtex[width=.15\linewidth]{boids_alignment} &
				{\Huge $+$} &
				\includegraphicswtex[width=.15\linewidth]{boids_cohesion} \\
				\small separation &	&
				\small alignment & &
				\small cohesion \\
			\end{tabular}
		\end{center}
\end{frame}

\sidecite{Rao95}
\begin{frame}[t]{BDI Architecture}
	\smaller
	\begin{description}
	\item[BDI] \colorizedfirstletters{alerted text}{Belief} \colorizedfirstletters{alerted text}{Desire} \colorizedfirstletters{alerted text}{Intention}
	\item[Beliefs] the informational state of the agent, in other words its beliefs about the world (including itself and other agents).
	\item[Goals] a desire that has been adopted for active pursuit by the agent.
	\item[Intentions] the deliberative state of the agent – what the agent has chosen to do.
	\item[Plans] sequences of actions for achieving one or more of its intentions.
	\item[Events] triggers for reactive activity by the agent.
	\item BDI may contribute to the too upper layers of the three-layer architecture. 
	\vspace{.25cm}
	\end{description}
	\begin{center}
		\includegraphics[width=.8\linewidth]{bdi_arch}
	\end{center}
\end{frame}

\sidecite{Harel87}
\begin{frame}[t,fragile]{State-Transition Diagrams}
	\begin{itemize}
	\item Define the behavior in terms of states (of the agent knowledge) and transitions.
	\item Actions may be trigerred on transitions or states.
	\item Markov models \citep{Baum66} or activity diagrams \citep{Rumbaugh99} may be used in place of state-transition diagrams.
	\end{itemize}
	\begin{columns}[t]
		\begin{column}{.5\linewidth}
			\begin{Tiny}
			\vspace{-2em}
			\begin{lstlisting}
enum State{NO_TRASH, TRASH_IN_FOV,
     TRASH_COLLECTED }

agent A {
  var state = NO_TRASH
  on Perception
     [state==NO_TRASH &&
      occurrence.containsTrash] {
   state = TRASH_IN_FOV
  }
  on Perception
     [state==NO_TRASH &&
      !occurrence.containsTrash] {
   randomMove
  }
  //...
}
			\end{lstlisting}
			\end{Tiny}
		\end{column}
		\begin{column}{.5\linewidth}
			\raisebox{-\height}{\includegraphics{uml_cleaner}}
		\end{column}
	\end{columns}
\end{frame}

\section[Physic Environment]{Simulation with a Physic Environment}

\subsection{General Principles}

\sidecite{GallandGaudDemangeKoukam2009_11}
\figureslide{{Situated Environment} Model}{environment_model}

\sidecite{GallandGaudDemangeKoukam2009_11}
\figureslide{Body-Mind Distinction}{body_mind}

\sidecite{Michel.07}
\begin{frame}{{Simultaneous Actions:} Influence-Reaction}
	\smaller
	\alertbox{How to support simultaneous actions from agents?}
	\begin{enumerate}
	\item An agent does not change the state of the environment directly.
	\item Agent gives a state-change expectation to the environment: the \emph{influence}.
	\item Environment gathers influences, and solves conflicts among them for obtaining its \emph{reaction}.
	\item Environment applies reaction for changing its state.
	\end{enumerate}
	\vspace{1em}
	\begin{center}
		\includegraphics[height=.4\textheight]{influencereaction}
	\end{center}
\end{frame}

\ifSPIMMODULE\else
\sidecite{GallandBalboGaudRodriguezPicardBoissier2015_780}
\begin{frame}[fragile]{{Physic Environment} in SARL}
	\begin{itemize}
	\item The agent has the capacity to use its body.
	\item The body supports the interactions with the environment.
	\end{itemize}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
event Perception {
  val object : Object
  val relativePosition : Vector
}

capacity EnvironmentInteraction {
  moveTheBody(motion : Vector)
  move(object : Object,
      motion : Vector)
  executeActionOn(object : Object,
      actionName : String,
      parameters : Object*)
}



space PhysicEnvironment {
  def move(object : Object,
          motion : Vector) {
   //...
  }
}
			\end{lstlisting}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
skill PhysicBody implements
      EnvironmentInteraction {

  val env : PhysicEnvironment

  val body : Object

  def moveTheBody(motion:Vector) {
   move(this.body, motion)
  }

  def move(object : Object,
          motion : Vector) {
   env.move(object, motion)
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
	%\putat(-13,26){\includeanimatedfigure[width=1.03\linewidth]{physic_env_frames}}
\end{frame}
\fi

\subsection{Example 1: Traffic Simulation}

\begin{frame}{Driving Activity}
	\begin{itemize}
	\item Each vehicle is simulated but road signs are skipped $\Rightarrow$ mesoscopic simulation.
	\item The roads are extracted from a Geographical Information Database.
	\end{itemize}
	\vspace{.25cm}
	\begin{itemize}
	\item The simulation model is composed of two parts \citep{GallandGaudDemangeKoukam2009_11}:
		\begin{enumerate}
		\item the environment: the model of the road network, and the vehicles.
		\item the driver model: the behavior of the driver linked to a single vehicle.
		\end{enumerate}
	\end{itemize}
	\begin{center}
		\zoombox{\includegraphics[width=.31\linewidth]{trip_pool}}
	\end{center}
\end{frame}

\sidecite{GallandGaudDemangeKoukam2009_11}
\begin{frame}{Model of the Environment}\smaller
	\begin{block}{Road Network}
		\begin{itemize}
		\item Road polylines: $S = \left\{ \langle path, objects \rangle \big| path = \langle (x_0,y_0) \cdots \rangle \right\}$
		\item Graph: $G = \left\{ S, S \mapsto S, S \mapsto S \right\} = \left\{ \text{segments}, \text{entering}, \text{exiting} \right\}$
		\end{itemize}
	\end{block}
	\begin{block}{Operations}
		\begin{itemize}
		\item Compute the set of objects perceived by a driver (vehicles, roads...):
			\[P = \left\{ o \Bigg|
				\begin{matrix}
				distance(d,o)\le \Delta \wedge \\
				o \in O \wedge \\
				\forall (s_1,s_2), path = s_1.\langle p, O \rangle.s_2
				\end{matrix}
				\right\}\]
			where $path$ is the roads followed by a driver $d$.
		\item Move the vehicles, and avoid physical collisions.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Architecture of the Driver Agent}
	\begin{center}
		\includegraphics[width=.45\linewidth]{agent_layers} \\[.25cm]
		\smaller\smaller\smaller Jasim model~\citep{GallandGaudDemangeKoukam2009_11}
	\end{center}
\end{frame}

\begin{frame}{Path Planning}
	\begin{itemize}
	\item Based on the A* algorithm \citep{Dechter:1985:GBS:3828.3830,RoutePlanningAlgorithms2009}:
		\begin{itemize}
		\item extension of the Dijkstra's algorithm: search shortest paths between the nodes of a graph.
		\item introduce the heuristic function $h$ to explore first the nodes that permits to converge to the target node.
		\end{itemize}
	\vfill
	\item Inspired by the D*-Lite algorithm \citep{Koenig.Dstarlite.2005}:
		\begin{itemize}
		\item A* family.
		\item supports dynamic changes in the graph topology and the values of the edges.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Collision Avoidance}
	\begin{itemize}
	\item \alert{Principle:} compute the acceleration of the vehicle to avoid collisions with the other vehicles.
	\item Intelligent Driver Model~\citep{PhysRevE.62.1805}
		{\smaller
		\[\code{followerDriving} = \begin{cases}
		- \dfrac{\left( v \Delta v \right)^2}{4b\Delta p^2} & \text{if the ahead object is far} \\
		- a \dfrac{\left( s + v w \right)^2}{\Delta p^2} & \text{if the ahead object is near} \\
		\end{cases}\]}
	\item Free driving:
		{\smaller
		\[\code{freeDriving} = a \left( 1 - \left(\frac{v}{v_c}\right)^4 \right)\]}
	\end{itemize}
\end{frame}

\sidenote{\url{http://www.voxelia.com}}
\begin{frame}{Highway Simulation}
	\begin{block}{What is simulated?}
		\begin{enumerate}
		\item Vehicles on a French highway.
		\item Danger event $\rightarrow$ ``an animal is crossing the highway and causes a crash''. 
		\item Alert events by GSM.
		\item Arrival of the security and rescue services.
		\end{enumerate}
	\end{block}
	\vfill
	\includegraphics[width=.6\linewidth]{uml_Aremis}\hfill\pgfuseimage{voxelialogo}
\end{frame}

\sidenote{\url{http://www.voxelia.com}}
\begin{frame}[t]{Video}
	\begin{center}
	\raisebox{-.5\height}{\embeddedvideo[width=.9\linewidth]{./video/aremis.avi}{aremis}}
	\end{center}
	\begin{center}
	\tiny Video done with the SIMULATE\textup{\regmark} tool --- 2012 \copyright Voxelia S.A.S
	\end{center}
\end{frame}

\subsection{Example 2: Pedestrian Simulation}

\sidenote{\url{http://www.voxelia.com}}
\begin{frame}{Pedestrian Simulation}
	\begin{block}{What is simulated?}
		\begin{enumerate}
		\item Movements of pedestrians at a microscopic level.
		\item Force-based model for avoiding collisions.
		\end{enumerate}
	\end{block}
	\vfill
	\citep{buisson.abmtrans13}\hfill\pgfuseimage{voxelialogo}
\end{frame}

\sidecite{buisson.abmtrans13}
\begin{frame}[b]{Force to Apply to Each Agent}
	\begin{columns}
		\begin{column}{.6\linewidth}
		The force to apply to each agent is:
		\[ \vec{F_a} = \vec{F} + w_a.\delta_{\|\vec{F}\|} \dfrac{\vec{p_t} - p_a}{\|\vec{p_t} - p_a\|} \]
		\[ \vec{F} = \sum_{i \in M} U(t_c^i) \cdot \hat{S_i} \]
		\begin{scriptsize}
		\begin{itemize}
		\item $\vec{F}$: collision-avoidance force.
		\item $\hat{S_i}$: a sliding force.
		\item $t_c^i$: time to collision to object $i$.
		\item $U(t)$: scaling function of the time to collision.
		\item $M$: set objects around (including the other agents).
		\item $w_a$: weight of the attractive force.
		\item $\delta_{x} g$: is $g$ if $x\leq0$, $0$ otherwise.
		\end{itemize}
		\end{scriptsize}
		\end{column}
		\begin{column}{.4\linewidth}
			\includegraphicswtex[width=\linewidth]{agent_forces}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Sliding Force}
	\begin{itemize}
	\item The sliding force $\vec{S_i}$ is:
		\[ \vec{s_j} = (p_j - p_a) \times \hat{y} \]
		\[ \hat{S_j} = \sgn (\vec{s_j} \cdot (\vec{p_t} - p_a)) \frac{\vec{s_j}}{\|\vec{s_j}\|} \]
	\item \smaller $\hat{y}$: vertical unit vector.
	\end{itemize}
	\vfill
	\begin{center}
		\includegraphicswtex[width=.65\linewidth]{sliding_force}
	\end{center}
\end{frame}

\begin{frame}{Scaling the Sliding Force}
	\begin{itemize}
	\item \alert{How to scale $\hat{S_j}$ to obtain the repulsive force?}
	\item Many force-based models use a monotonic decreasing function of the distance to an obstacle.
	\item But it does not support the velocity of the agent.
	\vfill
	\item \alert{Solution: Use time-based force scaling function.}
		\[ U(t) = \begin{cases}
				\frac{\sigma}{{t}^\phi} - \frac{\sigma}{{t_{max}}^\phi} & \text{if }0\leq t\leq t_{max} \\
				0 & \text{if }t > t_{max}
			\end{cases} \]
	\item \smaller $t$: estimated time to collision.
	\item $t_{max}$: the maximum anticipation time.
	\item  $\sigma$ and $\phi$ are constants, such that $U(t_{max}) = 0$.
	\end{itemize}
\end{frame}

\sidenote{\url{http://www.voxelia.com}}
\begin{frame}{Video (1/2)}
	\begin{center}
		\embeddedvideo[width=.6\linewidth]{./video/pedestrians_circle.avi}{pedestrians_circle}
	\end{center}
	\begin{center}
	\tiny Video done with the SIMULATE\textup{\regmark} tool --- 2014 \copyright Voxelia S.A.S
	\end{center}
\end{frame}

\sidenote{\url{http://www.voxelia.com}}
\begin{frame}{Video (2/2)}
	\begin{center}
		\embeddedvideo[width=.7\linewidth]{./video/gare.avi}{gare_belfort}
	\end{center}
	\begin{center}
	\tiny Video done with the SIMULATE\textup{\regmark} tool --- 2014 \copyright Voxelia S.A.S
	\end{center}
\end{frame}

\section{Conclusion}

\begin{frame}{{Key Elements} for this Chapter}
	\begin{description}
	\item[Key Concepts] Immersion, Interaction, Avatar, Animat, Shadow Avatar, Persistence, Environment Dynamics.
	\vspace{1em}
	\item[Key Modeling Approach] Multiagent Systems
	\vspace{1em}
	\item[Agent Concepts] Agent, Multiagent System, Environment.
	\vspace{1em}
	\item[Simulation] Simulator Architecture.
	\vspace{1em}
	\item[Design Principles] Simple behavior model, Complex behavior's perception.
	\end{description}
\end{frame}

\end{graphicspathcontext}

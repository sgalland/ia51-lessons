\begin{graphicspathcontext}{{./chapters/intro/1sarl/imgs/},{./chapters/intro/1sarl/imgs/auto/},\old}

\subsection[Programming MAS with SARL]{Programming Multiagent Systems with SARL}

\begin{frame}{{Programming Language} Evolution}
	\begin{block}{Agent: a new paradigm ?}
	\begin{itemize}
	\item Agent-Oriented Programming (AOP) reuses concepts and language artifacts from Actors and OOP.
	\item It also provides an higher-level abstraction than the other paradigms.
	\end{itemize}
	\end{block}
	\begin{center}
		\includegraphics[width=.8\linewidth]{programmation_paradigm}                           
	\end{center}
\end{frame}

\begin{frame}{{Design Principles} of SARL}
	\begin{block}{Language}
		\begin{itemize}
		\item \Emph{All agents are holonic (recursive agents).}
		\item There is not only one way of interacting but infinite.
		\item Event-driven interactions as the default interaction mode.
		\item Agent/environment architecture-independent.
		\item Massively parallel.
		\item Coding should be simple and fun.
		\end{itemize}
	\end{block}
	\begin{block}{Execution Platform}
		\begin{itemize}
		\item \alert{Clear separation between Language and Platform related aspects.}
		\item Everything is distributed, and it should be transparent.
		\item Platform-independent.
		\end{itemize}
	\end{block}
\end{frame}

\sidenote{This table was done according to experiments with my students and \cite{FeraudGalland2017_951.}}
\begin{frame}[b]{{Comparing SARL} to Other Frameworks}
	\tiny
	\begin{tabularx}{\linewidth}{|X|X|X|X|X|X|X|X|X|}
		\hline
		\tabularheading\chead{Name} & \chead{Domain} & \chead{Hierar.\textup{a}} & \chead{Simu.\textup{b}} & \chead{C.Phys.\textup{c}} & \chead{Lang.} & \chead{Beginners\textup{d}} & \chead{Free} \\
		\hline
		\hline
		GAMA & Spatial simulations & \checkmark & \checkmark & & GAML, Java & **[*] & \checkmark \\
		\hline
		Jade & General & & \checkmark & \checkmark & Java & * & \checkmark \\
		\hline
		Jason & General & & \checkmark & \checkmark & Agent\-Speaks & * & \checkmark \\
		\hline
		Madkit & General & & \checkmark & & Java & ** & \checkmark \\
		\hline
		NetLogo & Social/ natural sciences & & \checkmark & & Logo & *** & \checkmark \\
		\hline
		Repast & Social/ natural sciences & & \checkmark & & Java, Python, .Net & ** & \\
		\hline
		\rowcolor{CIADlightmagenta} SARL & General & \checkmark & \checkmark\textup{e} & \checkmark & SARL, Java, Xtend, Python & **[*] & \checkmark \\
		\hline
	\end{tabularx}
	\vspace{2em}
	\begin{enumerate}[a]
	\item Native support of hierarchies of agents.
	\item Could be used for agent-based simulation.
	\item Could be used for cyber-physical systems, or ambient systems.
	\item *: experienced developers; **: for Computer Science Students; ***: for others beginners.
	\item Ready-to-use Library: \textgoto{JaakLibrary}{\TINY Jaak Simulation Library}
	\end{enumerate}
\end{frame}

\ifSPIMMODULE\else
\begin{frame}[t,fragile]{Java vs. SARL}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}[language=java]
public class ExampleOfClass
   extends SuperClass
   implements SuperInterface {
  // Field
  private int a;
  // Single-initialization field
  private final String b
               = "example";
  // Constructor
  public ExampleOfClass(int p) {
   this.a = p;
  }
  // Function with return value
  public int getA() {
   return this.a;
  }
  // Simulation of default
  // parameter value
  public void increment(int a) {
   this.a += a;
  }
  public void increment() {
   increment(1);
  }
  // Variadic parameter
  public void add(int... v) {
   for(value : v) {
     this.a += value;
   }
  }
}
			\end{lstlisting}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class ExampleOfClass
   extends SuperClass
   implements SuperInterface {
  // Field
  var a : int
  // Single-initialization field
  // automatic detection of the
  // field type
  val b = "example"
  // Constructor
  new(p : int) {
   this.a = p
  }
  // Function with return value
  def getA : int {
   this.a
  }
  // Real default parameter value
  def increment(a : int = 1) {
   this.a += a
  }
  // Variadic parameter
  def add(v : int*) {
   for(value : v) {
     this.a += value
   }
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[t,fragile]{{Implicit Calls} to Getters and Setters}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{itemize}
			\item Calling getter and setter functions is verbose and annoying.
			\item Syntax for field getting and setting is better.
			\item SARL compiler implicitly calls the getter/setter functions when field syntax is used.
			\end{itemize}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example {
  private var a : int

  def getA : int {
   this.a
  }
  def setA(a : int) {
   this.a = a
  }
}

class Caller {
  def function(in : Example) {
   // Annoying calls
   in.setA(in.getA + 1)
   // Implicit calls by SARL
   in.a = in.a + 1
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
	\smaller\smaller\vspace{-1cm}
	\begin{itemize}
	\item With call: \code{variable.field}; SARL seach for:
		\begin{enumerate}
		\item the function \code{getField} defined in the variable's type,
		\item the accessible field \code{field}.
		\end{enumerate}
	\item If the previous syntax is left operand of assignment operator, SARL seach for:
		\begin{enumerate}
		\item the function \code{setField} defined in the variable's type,
		\item the accessible field \code{field}.
		\end{enumerate}
	\end{itemize}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[t,fragile]{Extension Methods}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{description}
			\item[Goal] Extension of existing types with new methods.
			\item[Tool] Extension methods.
			\item[Principe] The first argument could be externalized prior to the function name.
			\vspace{1em}
			\item Standard notation: \\
				{\smaller\code{function(value1, value2, value3)}}
			\vspace{.5em}
			\item Extension method notation: \\
				{\smaller\code{value1.function(value2, value3)}}
			\end{description}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example {

  // Compute the Leivenstein
  // distance between two
  // strings of characters
  def distance(s1 : String,
              s2 : String)
              : int {
   // Code
  }

  def standardNotation {
   var d = distance("abc", "abz")
  }

  def extensionMethodNotation {
   var d = "abc".distance("abz")
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[t,fragile]{Lambda Expressions}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{description}
			\item[Lambda expression] a piece of code, which is wrapped in an object to pass it around.
			\vspace{1em}
			\item Notation: \\
				{\smaller\smaller\code{[ paramName : paramType, ... | code ]}}
			\vspace{.5em}
			\item Parameters' names may be not typed. If single parameter, \code{it} is used as name.
			\item Parameters' types may be not typed. They are infered by the SARL compiler.
			\end{description}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example {
  def example1 {
   var lambda1 = [
         a : int, b : String |
         a + b.length ]
  }

  def example2 {
   var lambda2 = [ it.length ]
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[t,fragile]{Type for a Lambda Expression}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{itemize}
			\item Type for a lambda expression may be written with a SARL approach, or a Java approach.
			\vspace{1em}
			\item Let the example of a lambda expression with: \begin{itemize}
				\item two parameters, one int, one String, and
				\item a returned value of type int.
				\end{itemize}
			\vspace{1em}
			\end{itemize}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example {
  def example1 :
      (int, String) => String {
    return [
      a : int, b : String |
      a + b.length ]
  }

  def example2 :
     Function2<Integer, String,
               Integer> {
    return [
      a : int, b : String |
      a + b.length ]
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
	\begin{description}
	\item[SARL notation] \code{(int, String) => int}
	\item[Java notation] \code{Function2<Integer, String, Integer>}
	\end{description}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[c,fragile]{Externalization of Lambda Argument}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{description}
			\item[Problem] Giving a lambda expression as function's argument is not friendly (see \code{example1}).
			\item[Goal] Allow a nicer syntax.
			\vspace{1em}
			\item[Principle] If the last parameter is a lambda expression, it may be externalized after the function's arguments (see \code{example2}).
			\end{description}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example {

  def myfct(a : int, b : String,
            c : (int) => int) {
   // Code
  }

  def example1 {
    myfct(1, "abc", [ it * 2 ])
  }

  def example2 {
    myfct(1, "abc") [ it * 2 ]
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[c,fragile]{Special Instance Variables}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{itemize}
			\item Usually, the OO languages provide special instance variables.
			\vspace{1em}
			\item SARL provides: \begin{itemize}
				\item \code{this}: the instance of current type declaration (class, agent, behavior...)
				\item \code{super}: the instance of the inherited type declaration.
				\item \code{it}: an object that \emph{depends on the code context}.
				\end{itemize}
			\end{itemize}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example extends SuperType {

  var field : int

  def thisExample {
   this.field = 1
  }

  def superExample {
   super.myfct
  }

  def itExample_failure {
   // it is unknown in this
   // context
   it.field
  }

  def itExample_inLambda {
   // it means: current parameter
   lambdaConsumer [ it + 1 ]
  }

  def lambdaConsumer((int) => int)
  {}
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[c,fragile]{Type Operators}
	\vspace{-.5cm}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{description}
			\smaller
			\item[Type] Explicit naming a type may be done with the optional operator: \\
				\code{typeof(TYPE)}.
			\item[Casting] Dynamic change of the type of a variable is done with operator: \\
				\code{VARIABLE as TYPE}.
			\item[Instance of] Dynamic type testing is supported by the operator:
				\code{VARIABLE instanceof TYPE}. \\[1em]
				If the test is done in a if-statement, it is not neccessary to cast the variable inside the inner blocks. 
			\end{description}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{lstlisting}
class Example {

  def typeofExample {
   var t : Class<?>
   t = typeof(String)
   t = String
  }

  def castExample {
   var t : int
   t = 123.456 as int
  }

  def instanceExample(t:Object) {
   var x : int
   if (t instanceof Number) {
     x = t.intValue
   }
  }

}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[c]{Other Special Operators}
	\begin{itemize}
	\item SARL provides special operators in addition to the classic operators from Java or C++:
	\end{itemize}
	\smaller
	\smaller
	\begin{tabularx}{\linewidth}{|c|X|X|}
	\hline
	\tabularheading\chead{Operator} & \chead{Semantic} & \chead{Java equivalent} \\
	\hline
	\code{a == b} & Object equality test & \texttt{a.equals(b)} \\
	\hline
	\code{a != b} & Object inequality test & \texttt{!a.equals(b)} \\
	\hline
	\code{a === b} & Reference equality test & \texttt{a == b} \\
	\hline
	\code{a !== b} & Reference inequality test & \texttt{a != b} \\
	\hline
	\code{a <=> b} & Compare a and b & \texttt{Comparable} interface \\
	\hline
	\code{a .. b} & Range of values $[a, b]$ & n/a \\
	\hline
	\code{a ..< b} & Range of values $[a, b)$ & n/a \\
	\hline
	\code{a >.. b} & Range of values $(a, b]$ & n/a \\
	\hline
	\code{a ** b} & Compute $a^b$ & n/a \\
	\hline
	\code{a -> b} & Create a pair $(a,b)$ & n/a \\
	\hline
	\code{a ?: b} & If a is not null then a else b & \texttt{a == null ? b : a} \\
	\hline
	\code{a?.b} & If a is not null then a.b is called else a default value is used & \texttt{a == null ? defaultValue : a.b} \\
	\hline
	\code{if (a) b else c} & Inline condition & \texttt{a ? b : c} \\
	\hline
	\end{tabularx}
\end{frame}
\fi

\ifSPIMMODULE\else
\begin{frame}[c,fragile]{{Operator Definition} and Overriding}
	\smaller
	\begin{itemize}
	\item SARL allows overriding or definition operators.
	\item Each operator is associated to a specific function name that enables the developper to redefine the operator's code.
	\item Examples of operators in SARL:
	\end{itemize}
	\smaller
	\begin{tabularx}{\linewidth}{|c|c|X|}
	\hline
	\tabularheading\chead{Operator} & \chead{Function name} & \chead{Semantic} \\
	\hline
	\code{col += value} & operator\_add(Collection, Object) & Add an value into a collection. \\
	\hline
	\code{a ** b} & operator\_power(Number, Number) & Compute the power b of a. \\
	\hline
	\end{tabularx}
	\begin{columns}
		\begin{column}{.45\linewidth}
			\begin{lstlisting}
class Vector {
  var x : float
  var y : float
  new (x : float, y : float) {
   this.x = x ; this.y = y
  }
  def operator_plus(v: Vector)
     : Vector {
   new Vector(this.x + v.x,
              this.y + v.y)
  }
}
			\end{lstlisting}
		\end{column}
		\begin{column}{.45\linewidth}
			\begin{lstlisting}
class X {
  def fct {
   var v1 = new Vector(1, 2)
   var v2 = new Vector(3, 4)

   var v3 = v1 + v2
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}
\fi

\begin{frame}[c]{Overview of SARL Concepts}
	\begin{block}{Multiagent System in SARL}
	A \emph{collection of agents} interacting together in a collection of \emph{shared distributed spaces}.
	\end{block}

	\begin{scriptsize}
	\begin{columns}
		\begin{column}{0.25\linewidth}
			\begin{block}{4 main concepts}
			\begin{itemize}
			\item Agent
			\item Capacity
			\item Skill
			\item Space
			\end{itemize}
			\end{block}
		\end{column}
		\begin{column}{0.7\linewidth}
			\begin{block}{3 main dimensions}
			\begin{description}
			\item[Individual:] the Agent abstraction (Agent, Capacity, Skill)
			\item[Collective:] the Interaction abstraction (Space, Event, etc.)
			\item[Hierarchical:] the Holon abstraction (Context)
			\end{description}
			\end{block}
		\end{column}
	\end{columns}
	\begin{center}
		\textbf{SARL: a general-purpose agent-oriented programming language.} Rodriguez, S., Gaud, N., Galland, S. (2014) Presented at the The 2014 IEEE/WIC/ACM International Conference on Intelligent Agent Technology, IEEE Computer Society Press, Warsaw, Poland. \citep{rodriguez_sarl:_2014}	\\[1em]
		\Emph{\url{http://www.sarl.io}}
	\end{center}
	\end{scriptsize}
\end{frame}

\begin{frame}[fragile]{Agent}
	\begin{columns}
		\begin{column}[t]{.5\linewidth}
			\begin{block}{Agent}
			\begin{itemize}
			\item An agent is an autonomous entity having some intrinsic skills to implement the \alert{capacities} it exhibits. 
			\item An agent initially owns native capacities called \alert{Built-in Capacities}. 
			\item An agent defines a \alert{Context}.
			\end{itemize}
			\end{block}
		\end{column}
		\begin{column}[t]{.5\linewidth}
			\begin{center}
			\includegraphics[width=.9\linewidth]{agent_overview}
			\end{center}
			\begin{lstlisting}
agent HelloAgent {
  on Initialize {
   println("Hello World!")
  } 
  on Destroy {
   println("Goodbye World!")
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[fragile,t]{Example of Agent Code}
	\begin{center}
	\begin{lstlisting}[basicstyle=\small]
package org.multiagent.example

agent HelloAgent {

  var myvariable : int
  val myconstant = "abc"

  on Initialize {
    println("Hello World!")
  } 

  on Destroy {
    println("Goodbye World!")
  }

}
	\end{lstlisting}
	\end{center}
	\putat(16,52){\includeanimatedfigure[width=1.05\linewidth]{agent_frames}}
\end{frame}

\begin{frame}[t]{Capacity and Skill}
	\begin{columns}
		\begin{column}{.8\linewidth}
			\begin{scriptsize}  
			\begin{block}{Action}
			\begin{itemize}
			\item A specification of a transformation of a part of the designed system or its environment. 
			\item Guarantees resulting properties if the system before the transformation satisfies a set of constraints.
			\item Defined in terms of pre- and post-conditions.
			\end{itemize}
			\end{block}
			\begin{block}{Capacity}
			Specification of a collection of actions.
			\end{block}
			\begin{block}{Skill}
			A possible implementation of a capacity fulfilling all the constraints of its specification, the capacity.
			\end{block}
			\end{scriptsize}
		\end{column}
		\begin{column}{.2\linewidth}
			\includegraphics{uml_CapacitySkillRelation}
		\end{column}
	\end{columns}
	\vspace{.5cm}
	\alertbox{Enable the separation between a generic behavior and agent-specific capabilities.}
\end{frame}

\begin{frame}[fragile]{Example of Capacity and Skill}
	\begin{columns}
		\begin{column}{.5\linewidth}
			\begin{lstlisting}
capacity Logging {

  def debug(s : String)

  def info(s : String)

}


skill BasicConsoleLogging
      implements Logging {

  def debug(s : String) {
    println("DEBUG:" + s)
  }

  def info(s : String) {
    println("INFO:" + s)
  }

}
			\end{lstlisting}
		\end{column}
		\begin{column}{.5\linewidth}
			\begin{lstlisting}
agent HelloAgent {

  uses Logging

  on Initialize {
   setSkill(
     new BasicConsoleLogging)
   info("Hello World!")
  } 

  on Destroy {
   info("Goodbye World!")
  }

}
			\end{lstlisting}
		\end{column}
	\end{columns}
	\putat(8,47){\includeanimatedfigure[width=1.05\linewidth]{capacities_frames}}
\end{frame}

\begin{frame}{Space: interactions between agents}
	\begin{footnotesize} 
	\begin{block}{Space}
	Support of interaction between agents respecting the rules defined in various Space Specifications.
	\end{block}
	\begin{block}{Space Specification}
		\begin{itemize}
		\item Defines the rules (including action and perception) for interacting within a given set of Spaces respecting this specification.
		\item Defines the way agents are addressed and perceived by other agents in the same space.
		\item A way for implementing new interaction means.
		\end{itemize} 
	\end{block}.
	\alertbox{The spaces and space specifications must be written with an Object-Oriented Programming language, e.g. Java; or with the basic OO statements within SARL}
	\end{footnotesize}  
\end{frame}

\begin{frame}{Context and Spaces}
	\begin{columns}
		\begin{column}{.6\linewidth}
			\begin{footnotesize}  
			\begin{block}{Context}
				\begin{itemize}
				\item Defines the boundary of a sub-system.
				\item Collection of Spaces.
				\item Every Context has a \alert{Default Space}.
				\item Every Agent has a \alert{Default Context}, the context where it was spawned.
				\end{itemize}
			\end{block}
			\end{footnotesize}  
		\end{column}
		\begin{column}{.4\linewidth}
			\begin{center}
			\includegraphics[width=.9\linewidth]{ContextSpace}
			\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{{Default Space} in Every Contexts}
	\begin{columns}
		\begin{column}{.5\linewidth}
			\begin{footnotesize}  
			\begin{block}{Default Space: an Event Space}
				\begin{itemize}
				\item Event-driven interaction space.
				\item Default Space of a context, contains all agents of the considered context.
				\item Event: the specification of some \alert{occurrence} in a Space that may potentially trigger effects by a participant.
				\end{itemize}
			\end{block}
			\end{footnotesize}  
		\end{column}
		\begin{column}{.5\linewidth}
			\begin{center}
			\includegraphics{agent_interaction}
			\end{center}
		\end{column}
	\end{columns}
\end{frame}


\figureslide{{Example of Interactions:} Ping - Pong}{ping_pong_example}


\begin{frame}[fragile]{{Example of Interactions:} Ping - Pong}
	\begin{scriptsize}
	\begin{columns}[c]
		\begin{column}{.5\linewidth}
			\begin{lstlisting}
event Ping {
  var value : Integer
  new (v : Integer) {
   value = v
  }
}

event Pong {
  var value : Integer
  new (v : Integer) {
   value = v
  }
}

agent PongAgent {
  uses DefaultContextInteractions
  on Initialize {
   println("Waiting for ping")
  }
  on Ping {
   println("Recv Ping: "
      + occurrence.value)
   println("Send Pong: "
      + occurrence.value)
   emit(new Pong(
            occurrence.value))
  }
}
			\end{lstlisting}
		\end{column}
		\begin{column}{.5\linewidth}
			\begin{lstlisting}
agent PingAgent {
  uses Schedules
  uses DefaultContextInteractions
  var count : Integer
  on Initialize {
   println("Starting PingAgent")
   count = 0
   in(2000) [ sendPing ]
  }
  def sendPing {
   if (defaultSpace.
         participants.size > 1) {
     emit(new Ping(count))
     count = count + 1    
   } else {
     in(2000) [ sendPing ]
   }
  }
  on Pong {
   in(1000) [
     println("Send Ping: "+count)
     emit(new Ping(count))
     count = count + 1    
   ]
  }
}
			\end{lstlisting}
		\end{column}
	\end{columns}
	\end{scriptsize}
	%\putat(-12,27){\includeanimatedfigure[width=1.02\linewidth]{ping_pong_code}}
\end{frame}

\figureslide{{Execution} Sequence Diagram}{pingpong_sequence}

\begin{frame}[c]{Compatibility between SARL and Java}
	\alertbox{SARL is 100\% compatible with Java}
	\begin{center}
		\includegraphics[width=.9\linewidth]{sarl-compilation-process}
	\end{center}
	\begin{itemize}
	\item Any Java feature or library could be included and called from SARL.
	\item A Java application could call any public feature from the SARL API.
	\end{itemize}
\end{frame}

\begin{frame}[c]{Documentation of SARL}
	\begin{center}
		The SARL syntax is explained into the ``General Syntax Reference'' on the SARL website. \\
		\vspace{1cm}
		\url{http://www.sarl.io/docs/}
	\end{center}
\end{frame}

\subsection{Development Environment}

\figureslide{SARL in the Eclipse IDE}{eclipse_about}

\ifSPIMMODULE\else
\figureslide{SARL Perspective}{eclipse_java_perspective}
\fi

\ifSPIMMODULE\else
\figureslide{Create a SARL Project}{eclipse_new_project}
\fi

\ifSPIMMODULE\else
\figureslide{Create a SARL Project \insertcontinuationtext}{eclipse_new_project2}
\fi

\ifSPIMMODULE\else
\figureslide{Create a SARL Project \insertcontinuationtext}{eclipse_new_project3}
\fi

\ifSPIMMODULE\else
\figureslide{Create Your First Agent}{eclipse_new_agent}
\fi

\ifSPIMMODULE\else
\figureslide{Create Your First Agent \insertcontinuationtext}{eclipse_new_agent2}
\fi

\ifSPIMMODULE\else
\figureslide{Define the Execution Environment}{eclipse_runtime_env}
\fi

\ifSPIMMODULE\else
\figureslide{Executing the Agent with Janus}{eclipse_launch_config}
\fi

\ifSPIMMODULE\else
\figureslide{Executing the Agent with Janus \insertcontinuationtext}{eclipse_launch_config2}
\fi

\subsection{Execution Environment}

\begin{frame}{{Run-time Environment} for SARL}
	\smaller
	\begin{block}{Run-time Environment Requirements}	
	\begin{itemize}
		\item Implements SARL concepts.
		\item Provides Built-in Capacities.
		\item Handles Agent's Lifecycle.
		\item Handles resources.
	\end{itemize}
	\end{block}
	\begin{block}{Janus as a SARL Run-time Environment}
		\wrapfigure[width=1.5cm]{janus}
		\begin{itemize}
		\wrapitem{\alert{Fully distributed}.}
		\wrapitem{\alert{Dynamic discovery of Kernels}.}
		\wrapitem{\alert{Automatic synchronization of kernels' data (easy recovery)}.}
		\item Micro-Kernel implementation.
		\item Official website: \url{http://www.janusproject.io}
		\end{itemize}
	\end{block}
	\vspace{.25em}
	\alertbox{Other SREs may be created. See Chapter \#\ref{chap:platform}.}
\end{frame}

\end{graphicspathcontext}
